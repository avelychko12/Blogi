from django.utils.text import slugify


def unique_slugify(instance, text):
    slug = slugify(text)
    slug_template = slug + '-%d'
    index = 1

    Klass = instance.__class__

    while Klass.objects.filter(slug=slug).exists() and \
            Klass.objects.get(slug=slug) != instance:
        slug = slug_template % index
        index += 1

    return slug
