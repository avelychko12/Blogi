from django.contrib.auth.models import User

from rest_framework import serializers

from blog.models import Article, Category


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'password')

        extra_kwargs = {
            'password': {'write_only': True, 'min_length': 8},
        }

    def create(self, validated_data):
        user = super(UserSerializer, self).create(validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'name', 'slug')

        extra_kwargs = {
            'slug': {'read_only': True},
        }


class ArticleSerializer(serializers.ModelSerializer):
    categories = CategorySerializer(many=True, read_only=True)
    user = UserSerializer(many=False, read_only=True)
    categories_json = serializers.JSONField(write_only=True)

    class Meta:
        model = Article
        fields = ('id', 'user', 'title', 'slug', 'content', 'categories',
                  'categories_json', 'published', 'modified', 'image',)

        extra_kwargs = {
            'user': {'read_only': True},
            'slug': {'read_only': True},
            'modified': {'read_only': True},
        }

    def create(self, validated_data):
        categories = validated_data.pop('categories_json')
        validated_data['user'] = self.context['request'].user

        new_article = Article.objects.create(**validated_data)
        for category in categories:
            new_article.categories.add(Category.objects.get(pk=category['id']))

        return new_article
