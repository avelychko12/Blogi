from django.contrib.auth.models import User

from rest_framework.viewsets import ModelViewSet

from blog.models import Article, Category
from .serializers import ArticleSerializer, CategorySerializer, UserSerializer
from .permissions import IsOwnerOrReadOnly, IsStaffOrReadOnly, ToCreateForAnonymousOrReadOnly
from .pagination import ArticlePagination

class UserViewSet(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (ToCreateForAnonymousOrReadOnly, )


class CategoryViewSet(ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    permission_classes = (IsStaffOrReadOnly,)


class ArticleViewSet(ModelViewSet):
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer
    permission_classes = (IsOwnerOrReadOnly,)
    pagination_class = ArticlePagination

    def get_queryset(self):
        qs = Article.objects.all().order_by('-published')

        username = self.request.query_params.get('username', None)
        if username is not None:
            return qs.filter(user__username=username)

        category = self.request.query_params.get('category', None)
        if category is not None:
            return qs.filter(categories__slug=category)

        slug = self.request.query_params.get('slug', None)
        if slug is not None:
            return qs.filter(slug=slug)

        return qs
