from rest_framework import permissions


class IsOwnerOrReadOnly(permissions.IsAuthenticatedOrReadOnly):
    def has_object_permission(self, request, view, obj):
        return (
                request.method in permissions.SAFE_METHODS or
                obj.user == request.user
        )

    def has_permission(self, request, view):
        return super().has_permission(request, view)


class IsStaffOrReadOnly(permissions.IsAdminUser):
    def has_permission(self, request, view):
        return (
                request.method in permissions.SAFE_METHODS or
                super().has_permission(request, view)
        )


class ToCreateForAnonymousOrReadOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method == 'POST':
            return not request.user.is_authenticated
        return request.method in permissions.SAFE_METHODS
