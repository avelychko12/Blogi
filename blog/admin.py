from django.contrib import admin
from .models import Article, Category


class ArticleModelAdmin(admin.ModelAdmin):
    fields = ('user', 'title', 'slug', 'categories', 'content', 'image', 'published', 'modified')
    readonly_fields = ('slug', 'published', 'modified')


class CategoryModelAdmin(admin.ModelAdmin):
    fields = ('name', 'slug')
    readonly_fields = ('slug',)


admin.site.register(Article, ArticleModelAdmin)
admin.site.register(Category, CategoryModelAdmin)
