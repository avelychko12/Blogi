from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

from .utils import unique_slugify


class Category(models.Model):
    name = models.CharField(max_length=200)
    slug = models.SlugField(max_length=250)

    def save(self, *args, **kwargs):
        self.slug = unique_slugify(self, self.name)
        super(Category, self).save(*args, **kwargs)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Categories'


class Article(models.Model):
    user = models.ForeignKey(User, related_name='articles', on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    slug = models.SlugField(max_length=250, unique=True)
    content = models.TextField()
    published = models.DateTimeField(editable=False)
    modified = models.DateTimeField()
    image = models.ImageField(upload_to='article_images/', null=True, blank=True)

    categories = models.ManyToManyField(Category, related_name='articles', blank=True)

    def save(self, *args, **kwargs):
        self.slug = unique_slugify(self, self.title)
        if not self.id:
            self.published = timezone.now()
        self.modified = timezone.now()
        super(Article, self).save(*args, **kwargs)

    def __str__(self):
        return self.user.username + ' : ' + self.title
