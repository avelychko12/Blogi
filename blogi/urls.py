from django.urls import path, include, re_path
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

from blog import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include('blog.api.urls')),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns.append(re_path(r'(?P<path>.*)', views.index, name='index'))
