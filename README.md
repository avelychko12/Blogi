![Blogi logo](https://gitlab.com/avelychko12/Blogi/raw/master/blogi-ui/src/assets/logo/in-width/logo.png)

# Blogi Website

Technologies used:
 - Django
 - Django Rest Framework
 - PostgreSQL
 - VueJS

[Click to visit website](http://188.166.104.101/)