import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import App from './App.vue';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import axios from 'axios';
import VueAxios from 'vue-axios';
import VueRouter from 'vue-router';
import {routes} from './routes';
import {store} from './store';

axios.defaults.baseURL = 'https://127.0.0.1:8000';

Vue.use(BootstrapVue);
Vue.use(VueAxios, axios);
Vue.use(VueRouter);

const router = new VueRouter({routes, mode: 'history'});

new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
});
