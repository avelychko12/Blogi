import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import jwt_decode from 'jwt-decode';

Vue.use(Vuex);

axios.defaults.xsrfCookieName = 'csrftoken';
axios.defaults.xsrfHeaderName = 'X-CSRFToken';

export const store = new Vuex.Store({
    state: {
        username: localStorage.getItem('username'),
        jwt: localStorage.getItem('token'),
        endpoints: {
            baseUrl: 'https://127.0.0.1:8000/api/',
            obtainJWT: 'https://127.0.0.1:8000/api/auth/obtain_token/',
            refreshJWT: 'https://127.0.0.1:8000/api/auth/refresh_token/',
        }
    },

    mutations: {
        updateToken(state, token) {
            const username = jwt_decode(token).username;

            localStorage.setItem('token', token);
            localStorage.setItem('username', username);
            state.jwt = token;
            state.username = username;
        },
        removeToken(state) {
            localStorage.removeItem('token');
            localStorage.removeItem('username');
            state.jwt = null;
            state.username = null;
        }
    },

    actions: {
        refreshToken() {
            const payload = {
                token: this.state.jwt
            };
            axios.post(this.state.endpoints.refreshJWT, payload)
                .then(res => this.commit('updateToken', res.data.token))
                .catch(err => console.log(err));
        },
        inspectToken() {
            const token = this.state.jwt;
            if (!token) {
                return;
            }

            const decoded = jwt_decode(token);
            const exp = decoded.exp;
            const orig_iat = decoded.orig_iat;

            const currentTime = Date.now() / 1000;
            const EXPIRATION_DELTA = 1800;
            const REFRESH_EXPIRATION_DELTA = 628200;

            if (currentTime - orig_iat < REFRESH_EXPIRATION_DELTA) {
                if (exp - currentTime < EXPIRATION_DELTA / 2) {
                    this.dispatch('refreshToken');
                }
            } else {
                // PROMPT USER TO RE-LOGIN, THIS ELSE CLAUSE COVERS THE CONDITION WHERE A TOKEN IS EXPIRED AS WELL
                this.state.jwt = null;
                this.state.username = null;
                localStorage.removeItem('token');
                localStorage.removeItem('username');
            }

        }
    }
});