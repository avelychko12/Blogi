import ArticleList from './components/ArticleList.vue';
import Article from './components/Article.vue';
import ArticleForm from './components/ArticleForm.vue';
import UserPage from './components/UserPage.vue';

export const routes = [
    {path: '/new-article/', component: ArticleForm, },

    {path: '/article/:slug', component: Article,},

    {path: '/user/:username/:page', component: UserPage,},
    {path: '/user/:username', redirect: '/user/:username/1',},

    {path: '/:slug/:page', component: ArticleList,},
    {path: '/:slug/', redirect: '/:slug/1',},

    {path: '', redirect: '/all/1',},
];